<?php

/**
 * @file
 * Install, update and uninstall functions for the changelog module.
 */

function changelog_schema() {


    $schema = array();

    $schema['changelog'] = array(
        'description' => 'Records the history of submitted administrative form elements',
        'fields' => array(
            'id' => array(
                'description' => 'Row ID',
                'type' => 'serial',
                'unsigned' => TRUE,
                'not null' => TRUE,
            ),
            'form_id' => array(
                'description' => 'Drupal Form ID',
                'type' => 'varchar',
                'length' => 1024,
                'not null' => TRUE,
                'default' => '',
            ),
            'uid' => array(
                'description' => 'The {users}.uid of the user who submitted this form',
                'type' => 'int',
                'not null' => TRUE,
                'default' => 0
            ),
            'submitted' => array(
                'description' => 'The Unix timestamp when the form was most recently submitted.',
                'type' => 'int',
                'not null' => TRUE,
                'default' => 0
            ),
            'path' => array(
                'description' => "The Path this form was found on",
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 2048,
                'default' => '',
            ),
            'form_element' => array(
               'description' => "Form Element Modified",
               'type' => 'varchar',
               'not null' => TRUE,
                'length' => 256,
                'default' => '',
            ),
            'old_values' => array(
                'description' => "The old values of elements that were changed",
                'type' => 'varchar',
                'serialize' => TRUE,
                'length' => 4096,
                'default' => NULL,
            ),
            'new_values' => array(
               'description' => "The new values of elements that we changed",
                'type' => 'varchar',
                'serialize' => TRUE,
                'length' => 4096,
                'default' => NULL,
            ),
            'notes' => array(
                'description' => "Notes submitted by the user",
                'type' => 'varchar',
                'length' => 4096,
                'not null' => FALSE,
            ),
        ),
        'indexes' => array(
            'form_path' => array('form_id', 'path'),
            'uid' => array('uid'),
            'submitted' => array('submitted'),
        ),
        'primary key' => array('id'),
    );

    $schema['changelog_exclude'] = array(
        'description' => 'Lists forms that are excluded from tracking.',
        'fields' => array(
            'path' => array(
                'description' => "The Path this form was found on",
                'type' => 'varchar',
                'not null' => TRUE,
                'length' => 2048,
                'default' => TRUE,
            ),
        ),
    );

    return $schema;
}

function changelog_install() {
    drupal_install_schema('changelog');
}

function changelog_uninstall() {
    drupal_uninstall_schema('changelog');
}